#!/usr/bin/env python
__author__ = 'justingibbons'

from rdkit import Chem, DataStructs
#from rdkit.Chem.Fingerprints import FingerprintMols
from rdkit.Chem import MACCSkeys, Draw
from itertools import combinations
from rdkit.Chem import AllChem, SaltRemover
import csv

def get_data_from_sdf(infile):
    """Returns a SDMolSupplier object of the infile"""
    return Chem.SDMolSupplier(infile)

def mol_list_from_accession_smiles_csv(infile, header=False):
    """Create a list of mol objects from a file with the accession in the first column and the SMILES structure
    in the second column. Creates a property names 'accession' equal to the value in the first column.
    If header is True will skip the first line"""
    delimiter=","
    key="accession"
    name="_Name"
    mol_list=[]
    if header==True:
        header_found=False
    with open(infile) as in_csv:
        csv_reader=csv.reader(in_csv,delimiter=delimiter)
        for row in csv_reader:
            if header==False or header_found==True: #If there is no header or the header has been found parse the data
                acc=row[0].strip()
                smiles=row[1]
                mol=Chem.MolFromSmiles(smiles)
                mol.SetProp(key,acc)
                mol.SetProp(name,acc)
                mol_list.append(mol)
            else:
                header_found=True
                continue
    return mol_list


def calculate_MACCSkeys_fingerprints(molecules):
    """Takes in object containing the molecules and returns a list
        of the fingerprints for the molecules computed using the 166 public MACCS keys"""

    return [MACCSkeys.GenMACCSKeys(molecule) for molecule in molecules ]

def calculate_similarity(molecule1_fingerprint,molecule2_fingerprint,metric="Tanimoto"):
    """Calculates the similarity between molecule1_fingerprint and molecule2_fingerprint using the supplied metric.
    See rdkit documentation on similarityFunctions for the available similarity metrics.
    Forcing the calculation of the fingerprint before calculating the similarity because
    many comparisions will be made and it would be inefficient to recalculate the same
    fingerprint over and over."""

    #Note the nice non-branching way of doing this was failing when using Morgan similarity
    #so if you want to use different similarity you have to add a new branch.
    #This was probably caused by a bug in rdkit
    if metric=="Tanimoto":
        return DataStructs.TanimotoSimilarity(molecule1_fingerprint,molecule2_fingerprint)


def calculate_pairwise_maccskey_tanimoto(molecules,round_to=3):
    """Takes a list of molecule objects and calculates all of the pairwise tanimoto values. This isn't the most
    efficient way to do this with large numbers of molecules. This function exists to help me test more complicated
    but more computationally efficient ways of solving this problem. round_to is the number of decimal places to
    round to"""

    tanimoto_values= []
    fingerprints=calculate_MACCSkeys_fingerprints(molecules)
    for combos in combinations(fingerprints,2):
        tanimoto_values.append(round(calculate_similarity(combos[0],combos[1]),round_to))
    return tanimoto_values







def get_accession_id(molecule,accession_list=["PUBCHEM_COMPOUND_CID","CHEMBL ID","CHEMBL_ID","accession"]):
    """Attemts to retrieve_accession_ids from the sdf by search for the accession ids present in
    accession_list"""

    accession_not_found=True
    i=0
    number_of_accessions_to_try=len(accession_list)
    while accession_not_found:
        try:
            return molecule.GetProp(accession_list[i])
        except KeyError:
            if i+1 < number_of_accessions_to_try:
                i+=1
                continue
            else:
                raise KeyError("That accession is not present")


def return_canonical_smiles(smiles):
    """Takes a smiles as a string and returns the canonical form"""
    m=Chem.MolFromSmiles(smiles)
    return Chem.MolToSmiles(m)


class Compounds(dict):

    """Creates an object out of a list of rdkit compound objects that can be manipulated and studied as a group"""
    def __init__(self,compound_list,remove_salts=False):
        """Creates a dictionary of a dictionary with the primary key being the pubchem compound ID and intialzes
        the secondary dictionary with the rdkit object storing the compounds strutural information.
        if remove_salts is set to True the object is created with salts removed"""
        self.compound_dic=dict()
        self.rdkit_obj="rdkit_object"
        for molecule in compound_list:
            if remove_salts:
                remover=SaltRemover.SaltRemover()
                molecule=remover.StripMol(molecule)
            self.compound_dic[get_accession_id(molecule)]={"rdkit_object":molecule}




    def __str__(self):
        return str(self.compound_dic)


    def __getitem__(self, item):
        return self.compound_dic[item]

    def __iter__(self):
        return iter(self.compound_dic)

    def __len__(self):
        return len(self.compound_dic.keys())



    def MACCSkeys(self):
        """Generates the MACCS keys for all the of the molecules in the dictionary. Saves them
         in the subdictionary under "MACCSkey" """
        for cid in self.compound_dic:
            self.compound_dic[cid]["MACCSkey"] = MACCSkeys.GenMACCSKeys(self.compound_dic[cid]["rdkit_object"])

    def calculate_morgan_fingerprints(self,radius=2):
        """Calculates the Morgan fingerprint for each molecule in the dictioary and saves them in the subdictionary
        under "morgan_fingerprint_radius_inBits" """

        sub_dic_key="morgan_fingerprint_"+str(radius)
        for acc in self.compound_dic:
            self.compound_dic[acc][sub_dic_key]=AllChem.GetMorganFingerprint(self.compound_dic[acc][self.rdkit_obj],radius)


    def write_morgan_fingerprints(self,key="morgan_fingerprint_2",outfile="morgan_fingerprint_2.csv"):
        """Writes morgan fingerprint out as a bit string. Not sure if this will work. I've heard this is possible,
        but I don't really understand it"""
        with open(outfile, "w") as csv_out:
            csv_writer=csv.writer(csv_out,delimiter=",")


            for id in self.compound_dic:
                row=list(self.compound_dic[id][key].ToBitString())
                row.insert(0,id) #Put the id in the first column

                csv_writer.writerow(row)




    def calculate_pairwise_tanimoto(self,round_to=3,fingerprint="MACCSkey"):
        """Calculates all of the pairwise taniomoto scores using fingerprint and returns the results as a list of tuples.
        round_to is the number of decimal places to round the tanimoto coefficient to. Creates a list object
        in self that contains tuples of the pairs and their tanimoto coefficient"""
        cids=self.compound_dic.keys()
        self.pairwise_tanimotos=[]
        for pair in combinations(cids,2):
            cid1,cid2=pair
            taniomoto= round(calculate_similarity(self.compound_dic[cid1][fingerprint],self.compound_dic[cid2][fingerprint]),
                             round_to)
            self.pairwise_tanimotos.append((cid1,cid2,taniomoto))
        return self.pairwise_tanimotos

    def specific_pairwise_tanimoto(self,accessions_list,round_to=3,fingerprint="MACCSkey"):
        """Calculates the pairwise tanimotos for the compounds cooresponding to the accessions in the accession list.
        Returns a list of tuples like (acc1,acc2,tanimoto)"""
        tanimoto_list=[]

        for pair in combinations(accessions_list,2):
            acc1,acc2=pair
            tanimoto=round(calculate_similarity(self.compound_dic[acc1][fingerprint],self.compound_dic[acc2][fingerprint]),
                           round_to)

            tanimoto_list.append((acc1,acc2,tanimoto))
        return tanimoto_list

    def specific_pairwise_tanimoto_from_file(self,infile,round_to=3):
        """Takes a file where the accession numbers are grouped by lines and seperated by a space and returns a
        list of lists containing all the pairwise tanimotos for each group"""

        final_list=[]
        with open(infile,"r") as csv_in:
            csv_reader=csv.reader(csv_in,delimiter=" ")
            for group in csv_reader:
                final_list.append(self.specific_pairwise_tanimoto(group,round_to=round_to))
        return final_list


    def report_lowest_tanimoto_for_each_group(self,group_file,outfile,round_to=3):
        """Takes the groups in group_file and calculates all the pairwise tanimoto coefficients. Writes the
        lowest scoring pair to outfile as a csv with header "acc1 acc2 tanimoto_coefficient". Does not report ties for
        lowest tanimoto"""
        header=("acc1","acc2","tanimoto_coefficient")

        grouped_pairwise_tanimotos=self.specific_pairwise_tanimoto_from_file(group_file,round_to=3)
        with open(outfile,"w") as out:
            csv_writer=csv.writer(out,delimiter=",")
            for group in grouped_pairwise_tanimotos:
                group.sort(key=lambda tup: tup[2])
                csv_writer.writerow(group[0])

    def write_out_pairwise_tanimoto(self,outfile="tanimoto_coefficients.csv"):
        """Writes out the contents of self.pairwise_tanimotos to a comma delimited file. First
        line is the header: cid1,cid2,tanimoto_coefficient"""
        header=("cid1","cid2","tanimoto_coefficient")

        with open(outfile,"w") as csv_out:
            csv_writer=csv.writer(csv_out)
            csv_writer.writerow(header)
            for row in self.pairwise_tanimotos:
                csv_writer.writerow(row)

    def tanimoto_edge_attribute_file(self,outfile="tanimoto_coefficient_edge_atrributes_file.eda"):
        """Similar to write_out_pairwise_tanimoto but writes it out in the style of a cytoscape edge
        attribute file. This format isn't working with cytoscape well even though this is the format in
        the documentation."""

        header=["tanimoto_similarity"]
        interaction_type="(tanimoto_similarity)"


        with open(outfile, "w") as csv_out:
            csv_writer=csv.writer(csv_out,delimiter=" ")
            #Header written as first line to specify the interaction being designated by the file
            csv_writer.writerow(header)
            for row in self.pairwise_tanimotos:
                row=list(row) #turn it into a list so that the interaction type and '=' may be inserted.
                row.insert(1,interaction_type)
                row.insert(3, "=")
                csv_writer.writerow(row)

    def write_filtered_taniomoto_siff_file(self,outfile="filtered_tanimoto_pairs.sif",remove_less_than=0.5):
        """Creates a file in sif format of the compounds with tanimoto scores greater then or equal to remove_less_than"""
        interaction="similar_structure"
        with open(outfile,"w") as csv_out:
            csv_writer=csv.writer(csv_out,delimiter="\t")
            for row in self.pairwise_tanimotos:
                if row[2] >= remove_less_than:
                    new_row=(row[0],interaction,row[1])
                    csv_writer.writerow(new_row)
                else:
                    continue

    def write_maccskeys(self,outfile="macskeys.csv"):
        """Writes the Maccs keys vectors out to a csv. Note I don't know how to test this
        so I'm just checking to make sure it looks right"""
        
        with open(outfile, "w") as csv_out:
            csv_writer=csv.writer(csv_out,delimiter=",")
            
            
            for id in self.compound_dic:
                row=list(self.compound_dic[id]["MACCSkey"].ToBitString())
                row.insert(0,id) #Put the id in the first column
            
                csv_writer.writerow(row)

    def create_images(self,compound_keys,outfile,ncols=4,subImgSize=(200,200),legends="default"):
        """Creates an image from the compounds listed in compound_keys. ncols is the number of columns in the
        image. If legends left to default then compound_keys are used as the legends"""
        if legends=="default":
            legends=compound_keys
        molecules=[]
        for key in compound_keys:
            molecules.append(self.compound_dic[key]["rdkit_object"])
        img=Draw.MolsToGridImage(molecules,molsPerRow=ncols,subImgSize=subImgSize,legends=legends)
        img.save(outfile)

    def write_images(self,outdir=None):
        """Writes out all images to individual files named key.mol"""
        for key in self.compound_dic.keys():
            if outdir:
                outfile=outdir+"/"+key+".png"
            else:
                outfile=key+".png"
            self.create_images(compound_keys=[key],outfile=outfile,ncols=1)
    def create_group_image_files(self,group_list,basename_output_file="cluster_",ncols=4,subImgSize=(200,200),legends="default",file_extension="png"):
        """Takes a list containing lists of compound keys and writes out an image file for each list of compound keys.
        File is written as a base name with a number appended to it. The group numbers start at 1."""
        group_number=1
        for i in range(len(group_list)):
            group=group_list[i]
            if legends=="default":
                legend=group
            else:
                legend=legends[i]
            outfile=basename_output_file+str(group_number)+"."+file_extension
            self.create_images(group,outfile,ncols=ncols,subImgSize=subImgSize,legends=legend)
            group_number+=1


    def write_mol_files(self,outdir=None):
        """Writes molecules out to mol files with name key.mol."""

        for key in self.compound_dic:

            if outdir != None:
                outfile=outdir+"/"+key+".mol"
            else:
                outfile=key+".mol"

            with open(outfile,"w") as out:

                out.write(Chem.MolToMolBlock(self.compound_dic[key][self.rdkit_obj]))

    def write_3d_mol_files(self,outdir=None,remove_hydrogens=False):
        """Calculates a 3D conformation for the molecule and writes results to a mol file. The name of the
        file is key.mol. Default is to keep hydrogens from 3D structure"""
        for key in self.compound_dic:

            molec=self.compound_dic[key][self.rdkit_obj]

            #Calculate 2D Structure first
            AllChem.Compute2DCoords(molec)
            AllChem.EmbedMolecule(molec)

            #Add hydrogens to improve conformation accuracy
            molec=AllChem.AddHs(molec)
            AllChem.EmbedMolecule(molec)

            #Calculate the 3D conformation
            AllChem.UFFOptimizeMolecule(molec)

            if remove_hydrogens: #Remove hydrogens if neccessary
                molec=Chem.RemoveHs(molec)


            if outdir != None:
                outfile=outdir+"/"+key+".mol"
            else:
                outfile=key+".mol"

            with open(outfile,"w") as out:


                out.write(Chem.MolToMolBlock(molec))


    def filter_by_tanimoto(self,remove_less_than=0.5):
        """Creates a new Compounds object containing only the molecules that are part of a tanimoto pair with
        a similarity greater than or equal to remove_less_than"""


    # def write_accession_smiles_csv(self,outfile="accession_and_smiles.csv"):
    #     """Creates a CSV file with the accession number in the first column and the SMILES structure in the
    #     second column"""
    #     pass
    #     delimiter=","
    #     with open(outfile,"w") as csv_out:
    #         csv_writer=csv.writer(csv_out,delimiter=delimiter)
    #         for acc in self.compound_dic:
    #             row=(acc,Chem.MolToSmiles(self.compound_dic[acc]["rdkit_object"]))
    #             csv_writer.writerow(row)







if __name__ =="__main__":
    import unittest
    from compare_files_tools import get_file_lines   #Found in the misc_bin repository

    #Note there is no test for get_data_from_sdf.
    #There is not test for calculate_MACCSkeys_fingerprints but it must work
    #for calculate_similarity to work.
    #There is also no test for mol_list_from_accession_smiles_csv but the calculate_pairwise_tanimoto will work
    #if this is working.

    three_simple_molecules=[Chem.MolFromSmiles('CCOC'), Chem.MolFromSmiles('CCO'),Chem.MolFromSmiles('COC')]
    three_simple_molecules_with_cids=[]
    pubchem_ids= "test_data/3_samples.sdf"
    chembl_ids="test_data/three_chembl_cmpd.sdf"
    chembl_and_pubchem_ids="test_data/chembl_and_pubchem.sdf"
    acc_smiles_file="test_data/accession_smiles.csv"

    molecules_from_sdf=get_data_from_sdf(pubchem_ids)
    chembl_sdf=get_data_from_sdf(chembl_ids)
    chembl_compounds=Compounds(chembl_sdf)
    chembl_and_pubchem=get_data_from_sdf(chembl_and_pubchem_ids)
    compounds = Compounds(molecules_from_sdf)
    chembl_and_pubchem_compounds=Compounds(chembl_and_pubchem)
    acc_smiles_compound_obj=Compounds(mol_list_from_accession_smiles_csv(acc_smiles_file))

    class TestCalculateSimilarity(unittest.TestCase):


        def test_default_calculate_MACCSkeys_fingerprints_and_calculate_similarity(self):
            finger_prints=calculate_MACCSkeys_fingerprints(three_simple_molecules)
            self.assertEqual(calculate_similarity(finger_prints[0],finger_prints[1]),0.5)
            self.assertAlmostEqual(calculate_similarity(finger_prints[0],finger_prints[2]),0.538,places=3)
            self.assertAlmostEqual(calculate_similarity(finger_prints[1],finger_prints[2]),0.214,places=3)


        def test_len(self):
            self.assertEqual(len(compounds),3)



        def test_get_accession_id_pub_chem(self):
            cids=[]
            correct_cids=["15998580","4444231","2707"]
            for mol in molecules_from_sdf:
                cids.append(get_accession_id(mol))
            self.assertEqual(cids,correct_cids)

        def test_get_accession_id_chembl(self):
            ids=[]
            correct_ids=["CHEMBL35228","CHEMBL422330","CHEMBL1327821"]
            for mol in chembl_sdf:
                ids.append(get_accession_id(mol))
            self.assertEqual(ids,correct_ids)
        def test_get_accession_id_cat_file(self):
            """See if get_accession_id works on a file created by concatanating a file with pubchem ids and one
            with chembl ids"""
            ids=[]
            correct_ids=["CHEMBL35228","CHEMBL422330","CHEMBL1327821","15998580","4444231","2707"]
            for mol in chembl_and_pubchem:
                ids.append(get_accession_id(mol))
            self.assertEqual(ids,correct_ids)

        def test_return_canonical_smiles(self):
            self.assertEqual(return_canonical_smiles("[CH3][CH2][OH]"),"CCO")

        def test_get_pubchem_compound_cid_key_error(self):
            mol=three_simple_molecules[0]
            with self.assertRaises(KeyError):
                get_accession_id(mol)

        def test_calculate_pairwise_maccskey_tanimoto(self):
            correct_answer=[0.5,0.538,0.214]
            self.assertEqual(calculate_pairwise_maccskey_tanimoto(three_simple_molecules),correct_answer)

        # Start test for compound class


        def test_compound_init_pubchem_only(self):
            correct_keys_and_value_set1=set()
            values_in_compound_object_set1=set()
            for molecule in molecules_from_sdf:
                #Using Chem.MolToSmiles to convert the molecule data into a string representation of the molecule
                #to allow easy comparisions of the results.
                correct_keys_and_value_set1.add( (get_accession_id(molecule),Chem.MolToSmiles(molecule)))
            for cid in compounds:
                values_in_compound_object_set1.add((cid,Chem.MolToSmiles(compounds[cid]["rdkit_object"])))

            self.assertEqual(correct_keys_and_value_set1,values_in_compound_object_set1)

        def test_compound_init_chembl_and_pubchem(self):
            correct_keys_and_value_set1=set()
            values_in_compound_object_set1=set()
            for molecule in chembl_and_pubchem:
                correct_keys_and_value_set1.add( (get_accession_id(molecule),Chem.MolToSmiles(molecule) ))

            for id in chembl_and_pubchem_compounds:
                values_in_compound_object_set1.add((id, Chem.MolToSmiles(chembl_and_pubchem_compounds[id]["rdkit_object"])))
            self.assertEqual(values_in_compound_object_set1,correct_keys_and_value_set1)


        def test_compounds_calculate_pairwise_tanimoto(self):
            #First you need to generate the Maccs key fingerprints
            compounds.MACCSkeys()
            acc_smiles_compound_obj.MACCSkeys()
            self.assertEqual(set(compounds.calculate_pairwise_tanimoto()),set([('4444231', '2707',0.029),('4444231', '15998580',0.191)
                ,('2707', '15998580',0.111)]))

            self.assertEqual(set(acc_smiles_compound_obj.calculate_pairwise_tanimoto()),set([("1","2",0.5),("1","3",0.538),
                ("3","2",0.214)]))

        def test_calculate_morgan_fingerprints(self):
            #Get the test data
            morgan_fp_chem_file="test_data/morgan_fingerprint_file.csv"
            mfp_compounds=Compounds(mol_list_from_accession_smiles_csv(morgan_fp_chem_file))
            mfp_compounds.calculate_morgan_fingerprints()
            m1_fp=mfp_compounds["1"]["morgan_fingerprint_2"]
            m2_fp=mfp_compounds["2"]["morgan_fingerprint_2"]

            #Generate the correct answer
            m1_correct = Chem.MolFromSmiles('Cc1ccccc1')
            fp1_correct=AllChem.GetMorganFingerprint(m1_correct,2)
            m2_correct=Chem.MolFromSmiles("Cc1ncccc1")
            fp2_corect=AllChem.GetMorganFingerprint(m2_correct,2)
            self.assertTrue(m1_fp==fp1_correct and m2_fp==fp2_corect)

        def test_specific_pairwise_tanimoto(self):
            chembl_and_pubchem_compounds.MACCSkeys()
            self.assertEqual(set(chembl_and_pubchem_compounds.specific_pairwise_tanimoto(["15998580","4444231","2707"])),
                             set([('4444231', '2707',0.029),('15998580','4444231',0.191)
                ,('15998580','2707',0.111)]))

        def test_specific_pairwise_tanimoto_from_file(self):
            group_file="test_data/grouped_accessions.txt"
            correct_answer=[[('15998580','4444231',0.191),('15998580','2707',0.111),('4444231', '2707',0.029)],
                            [("CHEMBL1327821","CHEMBL35228",0.449),("CHEMBL1327821","CHEMBL422330",0.383),
                             ("CHEMBL35228","CHEMBL422330",0.479)]]
            chembl_and_pubchem_compounds.MACCSkeys()
            self.assertEqual(chembl_and_pubchem_compounds.specific_pairwise_tanimoto_from_file(group_file),correct_answer)

        def test_report_lowest_tanimoto_for_each_group(self):
            group_file="test_data/grouped_accessions.txt"
            ref_file="test_data/out_files/ref_report_lowest_tanimoto_for_each_group.csv"
            outfile="test_data/out_files/test_report_lowest_tanimoto_for_each_group.csv"
            chembl_and_pubchem_compounds.MACCSkeys()
            chembl_and_pubchem_compounds.report_lowest_tanimoto_for_each_group(group_file,outfile)
            result,ref=get_file_lines(outfile,ref_file)
            self.assertEqual(result,ref)

        def test_write_out_pairwise_taniomoto(self,outfile="test_data/out_files/test_write_out_pairwise_taniomoto.csv"):
            ref_file="test_data/out_files/ref_write_out_pairwise_taniomoto.csv"
            compounds.calculate_pairwise_tanimoto()
            compounds.write_out_pairwise_tanimoto(outfile)
            result,correct_result=get_file_lines(outfile,ref_file)
            self.assertEqual(result,correct_result)

        def test_tanimoto_edge_attribute_file(self,outfile="test_data/out_files/test_tanimoto_edge_attribute_file.eda"):
            ref_file="test_data/out_files/ref_tanimoto_edge_attribute_file.eda"
            compounds.tanimoto_edge_attribute_file(outfile)
            result,correct_result=get_file_lines(outfile,ref_file)
            self.assertEqual(result,correct_result)

        def test_write_tanimoto_siff_file(self,outfile="test_data/out_files/test_tanimoto_edge_attribute_file.siff"):
            ref_file="test_data/out_files/ref_tanimoto_edge_attribute_file.siff"
            compounds.write_filtered_taniomoto_siff_file(outfile,remove_less_than=0.10)
            result,correct_result=get_file_lines(outfile,ref_file)
            self.assertEqual(result,correct_result)

        def test_write_maccs_keys(self,outfile="test_data/out_files/write_maccs_keys_output.csv"):
            print "test_write_macs_keys just checks for errors. Does not prove output is correct"
            compounds.write_maccskeys(outfile)

        def test_create_images(self):
            print "test_create_images checks for errors. Does not prove output is correct"
            compounds.create_images(["15998580","4444231","2707"],"test_data/out_files/test_create_images.png",ncols=3)

        def test_create_group_image_files(self):
            print "test_create_group_image_files checks for errors. Does not prove output is correct"
            chembl_and_pubchem_compounds.create_group_image_files([["15998580","4444231","2707"],["CHEMBL35228","CHEMBL422330","CHEMBL1327821"]],
                                               basename_output_file="test_data/out_files/test_create_group_image_files_output_")

        def test_write_mol_files(self):
            compounds.write_mol_files(outdir="test_data/out_files")
            outfile1="test_data/out_files/2707.mol"
            outfile2="test_data/out_files/4444231.mol"
            outfile3="test_data/out_files/15998580.mol"

            #Check to see if being written out correclty
            #Convert to SMILES to allow comparision between compounds
            mol1=Chem.MolFromMolFile(outfile1)
            self.assertEqual(Chem.MolToSmiles(mol1),Chem.MolToSmiles(compounds["2707"]["rdkit_object"]))

            mol2=Chem.MolFromMolFile(outfile2)
            self.assertEqual(Chem.MolToSmiles(mol2),Chem.MolToSmiles(compounds["4444231"]["rdkit_object"]))

            mol3=Chem.MolFromMolFile(outfile3)
            self.assertEqual(Chem.MolToSmiles(mol3),Chem.MolToSmiles(compounds["15998580"]["rdkit_object"]))


        def test_write_3d_mol_files(self):
            mol=Chem.MolFromSmiles('C1CCC1')
            mol.SetProp("_Name","cyclobutane")
            mol.SetProp("accession","cyclobutane")
            obj=Compounds([mol])
            obj.write_3d_mol_files(outdir="test_data/out_files",remove_hydrogens=True)
            result_file="test_data/out_files/cyclobutane.mol"
            ref_file="test_data/out_files/ref_write_3d_mol_files.mol"
            result,ref=get_file_lines(result_file,ref_file)
            self.assertEqual(result,ref)





        # def test_write_accession_smiles_csv(self,outfile="test_data/out_files/test_write_accession_smiles_csv.csv"):
        #     compounds.write_accession_smiles_csv(outfile)
        #     ref="test_data/out_files/ref_write_out_accession_and_smiles.csv"
        #     result,correct_result=get_file_lines(outfile,ref)
        #
        #     self.assertEqual(result,correct_result)
        #     print "The SMILES written out by the RDKIT seems to differ from the one reported on pubchem"





    unittest.main()





