__author__ = 'justingibbons'
from csv_to_json import csv_to_json
from json_utils import write_json_to_csv
from rdkit import Chem
from rdkit.Chem import MACCSkeys
from calculate_similarity import calculate_MACCSkeys_fingerprints,calculate_similarity
def filter_on_smiles(file_smiles_to_keep,file_to_be_filered,outfile_in_smiles_to_keep,outfile_not_in_smiles_to_keep,
                     min_tanimoto=1,delimiter=","):
    """Takes 2 infiles. A csv file with a "Structure" heading with smiles below it and another csv with the
        smiles structures saved under the heading "SMILES". It writes the rows from file_to_be_filtered to
        outfile_in_smiles_to_keep if there is a smiles in the file_smiles_to_keep with a tanimoto greater than or equal
        to min_taniomoto, otherwise writes the row to outfile_not_in_smiles_to_keep"""

    file_smiles_to_keep_structure_key="Structure"
    file_to_be_filtered_structure_key="SMILES"

    structures_to_keep_as_molecule_objects=[] #Needed to calculate the fingerprints
    rows_in_smiles_to_keep=[] #Store the rows that at least a min_tanimoto with one of the structures in file_smiles_to_keep
    rows_not_in_smiles_to_keep=[]
    #Get the data from the files
    smiles_to_keep_data=csv_to_json(file_smiles_to_keep,delimiter=delimiter)
    to_be_filtered_data=csv_to_json(file_to_be_filered,delimiter=delimiter)

    #Create a list of fingerprints for the smiles_to_keep_data
    for row in smiles_to_keep_data:
        smiles=row[file_smiles_to_keep_structure_key].strip()
        molecule_object=Chem.MolFromSmiles(smiles)
        structures_to_keep_as_molecule_objects.append(molecule_object)

    structures_to_keep_fingerprints=calculate_MACCSkeys_fingerprints(structures_to_keep_as_molecule_objects)

    #Compare the fingerprints for the smiles in file_to_be_filtered to file_smiles_to_keep if the similarity
    #is at least min_tanimoto add the row to rows_to_keep to be written out latter
    for row in to_be_filtered_data:
        match_found=False #If no match is found want to save row to a different list
        smiles=row[file_to_be_filtered_structure_key].strip()
        molecule_object=Chem.MolFromSmiles(smiles)
        fingerprint1=MACCSkeys.GenMACCSKeys(molecule_object)
        for fingerprint2 in structures_to_keep_fingerprints:
            if calculate_similarity(fingerprint1,fingerprint2) >= min_tanimoto:
                rows_in_smiles_to_keep.append(row)
                match_found=True
                break #only interested if there is any match. Don't need to go through everything
            else:
                continue
        if match_found==False: #If no match found save to a different list
            rows_not_in_smiles_to_keep.append(row)
    write_json_to_csv(rows_in_smiles_to_keep,outfile_in_smiles_to_keep,to_be_filtered_data.header)
    write_json_to_csv(rows_not_in_smiles_to_keep,outfile_not_in_smiles_to_keep,to_be_filtered_data.header)




if __name__=="__main__":
    import unittest
    from compare_files_tools import get_file_lines

    class TestFilterOnSmiles(unittest.TestCase):
        def test_filter_on_smiles(self):
            file_smiles_to_keep="test_data/filter_on_smiles/filter_on_smiles_smiles_to_keep.csv"
            file_to_be_filtered="test_data/filter_on_smiles/filter_on_smiles_file_to_be_filtered.csv"
            ref_file="test_data/filter_on_smiles/ref_filter_on_smiles.csv"
            ref_not_in_smiles_file="test_data/filter_on_smiles/ref_filter_on_smiles_not_in_smiles.csv"
            outfile_in_smiles="test_data/filter_on_smiles/result_filter_on_smiles.csv"
            outfile_not_in_smiles="test_data/filter_on_smiles/result_not_in_smiles.csv"

            filter_on_smiles(file_smiles_to_keep,file_to_be_filtered,outfile_in_smiles,outfile_not_in_smiles)

            result_in_smiles,ref_in_smiles=get_file_lines(ref_file,outfile_in_smiles)
            result_not_in_smiles,ref_not_in_smiles=get_file_lines(outfile_not_in_smiles,ref_not_in_smiles_file)

            self.assertEqual(result_in_smiles,ref_in_smiles)
            self.assertEqual(result_not_in_smiles,ref_not_in_smiles)


    unittest.main()




